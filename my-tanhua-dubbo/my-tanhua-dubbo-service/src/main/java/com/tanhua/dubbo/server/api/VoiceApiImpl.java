package com.tanhua.dubbo.server.api;

import com.alibaba.dubbo.config.annotation.Service;
import com.mongodb.client.result.DeleteResult;
import com.tanhua.dubbo.server.pojo.FollowUser;
import com.tanhua.dubbo.server.pojo.Voice;
import com.tanhua.dubbo.server.service.IdService;
import com.tanhua.dubbo.server.vo.PageInfo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service(version = "1.0.0")
public class VoiceApiImpl implements VoiceApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private IdService idService;

    @Override
    public String saveVoice(Voice voice) {

        if (voice.getUserId() == null) {
            return null;
        }

        voice.setId(ObjectId.get());
        voice.setCreated(System.currentTimeMillis());
        //自增长id
        voice.setVid(this.idService.createId("voice", voice.getId().toHexString()));
        this.mongoTemplate.save(voice);

        return voice.getId().toHexString();
    }

    @Override
    public List<Voice> queryVoiceList() {
        return this.mongoTemplate.findAll(Voice.class);
    }
}
