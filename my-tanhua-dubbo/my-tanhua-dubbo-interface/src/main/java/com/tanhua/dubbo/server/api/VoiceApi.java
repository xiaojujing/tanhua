package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.Voice;

import java.util.List;

public interface VoiceApi {

    /**
     * 保存音频
     *
     * @param voice
     * @return
     */
    String saveVoice(Voice voice);

    /**
     * 查询所有音频列表
     *
     * @param
     * @return
     */
    List<Voice> queryVoiceList();

}
