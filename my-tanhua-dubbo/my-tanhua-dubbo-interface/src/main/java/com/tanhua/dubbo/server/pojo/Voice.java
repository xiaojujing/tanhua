package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "voice")
public class Voice implements java.io.Serializable {

    private static final long serialVersionUID = -3136732836884933873L;
    private ObjectId id; //主键id
    private Long vid;
    private Long userId;
    private String voiceUrl; //音频文件
    private Long created; //创建时间
}
