package com.tanhua.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 谢安吉
 * @date 2020-12-14 10:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sound {
    private Long id;
    private Long userId;
    private String soundUrl;
}
