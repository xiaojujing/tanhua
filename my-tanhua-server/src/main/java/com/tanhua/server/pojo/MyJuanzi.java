package com.tanhua.server.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyJuanzi {
    private String id;
    private String name;
    private String cover;
    private String level;
    private Integer star;
    private Integer isLock;
    private String reportId;
}
