package com.tanhua.server.controller;


import com.alibaba.dubbo.common.utils.StringUtils;
import com.tanhua.server.service.VoiceService;
import com.tanhua.server.vo.VoiceRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
//@RequestMapping("peachblossom")
public class VoiceController {

    @Autowired
    private VoiceService voiceService;

    //发送语音并保存
    @PostMapping
    public ResponseEntity<Void> saveVoice(@RequestParam(value = "soundFile", required = false) MultipartFile soundFile) {
        try {
            String id = this.voiceService.saveVoice(soundFile);
            if (StringUtils.isNotEmpty(id)) {
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    /**
     * 探花
     *
     * @return
     */
    @GetMapping
    public ResponseEntity<VoiceRandom> queryVoice() {
        try {
            VoiceRandom voiceRandom = this.voiceService.queryVoiceRandom();
            return ResponseEntity.ok(voiceRandom);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

//    /**
//     * 喜欢
//     *
//     * @param likeUserId
//     * @return
//     */
//    @GetMapping("{id}/love")
//    public ResponseEntity<Void> likeUser(@PathVariable("id") Long likeUserId) {
//        try {
//            this.todayBestService.likeUser(likeUserId);
//            return ResponseEntity.ok(null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
//    }
//
//    /**
//     * 不喜欢
//     *
//     * @param likeUserId
//     * @return
//     */
//    @GetMapping("{id}/unlove")
//    public ResponseEntity<Void> disLikeUser(@PathVariable("id") Long likeUserId) {
//        try {
//            this.todayBestService.disLikeUser(likeUserId);
//            return ResponseEntity.ok(null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
//    }


}
