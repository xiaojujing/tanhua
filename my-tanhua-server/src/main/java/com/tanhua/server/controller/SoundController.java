package com.tanhua.server.controller;

import com.tanhua.server.service.SoundService;
import com.tanhua.server.vo.SoundVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("peachblossom")
public class SoundController {

    @Autowired
    private SoundService soundService;
    @GetMapping
    public ResponseEntity<SoundVo> getVoice(){
        try {

            SoundVo soundVo = this.soundService.getSound();
            return ResponseEntity.ok(soundVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

    }

    @PostMapping
    public ResponseEntity<Void> saveSound(@RequestParam(value = "soundFile", required = false) MultipartFile soundFile) {
        try {
            this.soundService.saveSound(soundFile);
                return ResponseEntity.ok(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

}
