package com.tanhua.server.controller;


import com.tanhua.server.pojo.Answer;
import com.tanhua.server.service.JuanziService;
import com.tanhua.server.vo.JuanziVo;
import com.tanhua.server.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("testSoul")
public class JuanziController {

    @Autowired
    private JuanziService juanziService;

    @GetMapping
    public ResponseEntity<List<JuanziVo>> queryJuanziList() {
        try {
            List<JuanziVo> list = this.juanziService.queryJuanzisList();
            return ResponseEntity.ok(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @GetMapping("report/{id}")
    public ResponseEntity<ResultVo> quertResultReport(@PathVariable("id") Long id){
        try {
            ResultVo resultVo = this.juanziService.quertResultReport(id);
            return ResponseEntity.ok(resultVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @PostMapping
    public ResponseEntity<String> quertPush(@RequestBody Answer answer){
        try {
            String s = this.juanziService.queryPush(answer);
            return ResponseEntity.ok(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
