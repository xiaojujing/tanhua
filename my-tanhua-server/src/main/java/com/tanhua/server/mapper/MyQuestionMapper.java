package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.MyQuestion;

public interface MyQuestionMapper extends BaseMapper<MyQuestion> {
}
