package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.UserRecord;

public interface UserRecordMapper extends BaseMapper<UserRecord> {
}
