package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.Sound;

/**
 * @author 谢安吉
 * @date 2020-12-14 10:33
 */
public interface SoundMapper extends BaseMapper<Sound> {
}
