package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SoulTest {
    private String id; //问卷编号
    private String name;  //问卷名称
    private String cover;  //封面
    private String level;  //级别
    private Integer star;  //星别
    private Object[] questions;  //试题
}
