package com.tanhua.server.vo;

import com.tanhua.server.pojo.MyQuestion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JuanziVo {
    private String id;
    private String name;
    private String cover;
    private String level;
    private Integer star;
    private List<QuestionVo> questions;
    private Integer isLock;
    private String reportId;
}
