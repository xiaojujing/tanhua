package com.tanhua.server.vo;

import com.tanhua.server.pojo.Dimension;
import com.tanhua.server.pojo.SimilarYou;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultVo {
    private String conclusion;
    private String cover;
    private List<Dimension> dimensions;
    private List<SimilarYou> similarYous;
}
