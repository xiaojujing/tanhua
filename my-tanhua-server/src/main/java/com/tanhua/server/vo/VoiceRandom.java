package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 本次随机语音
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoiceRandom {

    private Long id;    //语音的id
    private String avatar;
    private String nickname;
    private String gender; //性别 man woman
    private Integer age;
    private String soundUrl;  //语音地址
    private Integer remainingTimes; //剩余次数

}
