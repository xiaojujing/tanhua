package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 谢安吉
 * @date 2020-12-13 16:10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoundVo {
    private Integer id;
    private String avatar; //头像
    private String nickname; //昵称
    private String gender;  //性别
    private Integer age;     //年龄
    private String soundUrl; //音频URL
    private Integer remainingTimes; //剩余次数
}
