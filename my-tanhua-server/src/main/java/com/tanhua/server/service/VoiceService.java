package com.tanhua.server.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.tanhua.dubbo.server.api.VoiceApi;
import com.tanhua.dubbo.server.pojo.Voice;
import com.tanhua.server.enums.SexEnum;
import com.tanhua.server.mapper.UserRecordMapper;
import com.tanhua.server.pojo.User;
import com.tanhua.server.pojo.UserInfo;
import com.tanhua.server.pojo.UserRecord;
import com.tanhua.server.utils.UserThreadLocal;
import com.tanhua.server.vo.PageResult;
import com.tanhua.server.vo.VoiceRandom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class VoiceService {

    @Reference(version = "1.0.0")
    private VoiceApi voiceApi;

    @Autowired
    private UserRecordMapper userRecordMapper;

    @Autowired
    protected FastFileStorageClient storageClient;

    @Autowired
    private FdfsWebServer fdfsWebServer;

    @Autowired
    private UserInfoService userInfoService;

    public String saveVoice(MultipartFile voiceFile) {
        User user = UserThreadLocal.get();
        Voice voice = new Voice();
        voice.setUserId(user.getId());
        try {
            //上传音频
            StorePath storePath = storageClient.uploadFile(voiceFile.getInputStream(),
                    voiceFile.getSize(),
                    StringUtils.substringAfter(voiceFile.getOriginalFilename(), "."),
                    null);
            voice.setVoiceUrl(fdfsWebServer.getWebServerUrl() + storePath.getFullPath());

            return this.voiceApi.saveVoice(voice);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public VoiceRandom queryVoiceRandom() {
        User user = UserThreadLocal.get();
        VoiceRandom voiceRandom = new VoiceRandom();
        List<Voice> voices = voiceApi.queryVoiceList();
        Random random = new Random();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",user.getId().intValue());
        int index;
        while (true){
            index = random.nextInt(10);
            List<UserRecord> list = this.userRecordMapper.selectList(queryWrapper);
            int flag=0; //没有重复
            for (int i = 0; i < list.size(); i++) {
                int now = list.get(i).getVoiceId();
                if(index==now-1){
                    flag=1; //已经推荐过了
                    break;
                }
            }
            if(flag==1) continue;
            else break;
        }
        Voice voice = voices.get(index);
        long cur = System.currentTimeMillis();
        long pre = voice.getCreated();
        int day= 1*24*60*60;   //一天有多少秒
        int time= (int) ((cur-pre)/1000);
//        if(time%day==0){       //当前时间过了一天
            voiceRandom.setRemainingTimes(9);
//            QueryWrapper queryWrapper_ = new QueryWrapper();
//            queryWrapper_.eq("user_id",user.getId());
//            userRecordMapper.delete(queryWrapper);
//        }else{
//            voiceRandom.setRemainingTimes(voiceRandom.getRemainingTimes()-1);
//        }
        voiceRandom.setId(voice.getVid());
        voiceRandom.setSoundUrl(voice.getVoiceUrl());
        UserInfo userInfo = userInfoService.queryUserInfoById(voice.getUserId());
        voiceRandom.setAge(userInfo.getAge());
        voiceRandom.setAvatar(userInfo.getCoverPic());
        voiceRandom.setGender(userInfo.getSex().toString());
        voiceRandom.setNickname(userInfo.getNickName());


        UserRecord userRecord = new UserRecord(null,user.getId().intValue(),voice.getVid().intValue());
        userRecordMapper.insert(userRecord);
        return voiceRandom;
    }

}
