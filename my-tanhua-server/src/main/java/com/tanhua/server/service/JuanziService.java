package com.tanhua.server.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.server.mapper.MyJuanziMapper;
import com.tanhua.server.mapper.MyOptionMapper;
import com.tanhua.server.mapper.MyQuestionMapper;
import com.tanhua.server.pojo.*;
import com.tanhua.server.utils.UserThreadLocal;
import com.tanhua.server.vo.JuanziVo;
import com.tanhua.server.vo.OptionVo;
import com.tanhua.server.vo.QuestionVo;
import com.tanhua.server.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class JuanziService {
    @Autowired
    private MyJuanziMapper myJuanziMapper;
    @Autowired
    private MyQuestionMapper myQuestionMapper;
    @Autowired
    private MyOptionMapper myOptionMapper;

    public List<JuanziVo> queryJuanzisList() {
        User user = UserThreadLocal.get();
        //初始查询条件
        List<String> juanziId = new ArrayList<>();
        juanziId.add("first");
        juanziId.add("second");
        juanziId.add("third");
        ArrayList<JuanziVo> result = new ArrayList<>();
        for (int i = 0; i < juanziId.size(); i++) {
            JuanziVo juanziVo = new JuanziVo();
            QueryWrapper queryWrapper1 = new QueryWrapper();
            queryWrapper1.eq("id", juanziId.get(i));
            //查询当前试卷，封装信息
            MyJuanzi myJuanzi = myJuanziMapper.selectOne(queryWrapper1);
            juanziVo.setId(myJuanzi.getId());
            juanziVo.setName(myJuanzi.getName());
            juanziVo.setCover(myJuanzi.getCover());
            juanziVo.setLevel(myJuanzi.getLevel());
            juanziVo.setStar(myJuanzi.getStar());
            juanziVo.setIsLock(myJuanzi.getIsLock());
            juanziVo.setReportId(myJuanzi.getReportId());
            //查询该套卷子下的所有试题
            QueryWrapper queryWrapper2 = new QueryWrapper();
            queryWrapper2.eq("uid", myJuanzi.getId());
            List<MyQuestion> questionlist = myQuestionMapper.selectList(queryWrapper2);
            ArrayList<QuestionVo> questions = new ArrayList<>();
            for (MyQuestion myQuestion : questionlist) {
                QuestionVo questionVo = new QuestionVo();
                //封装该题题干
                questionVo.setId(myQuestion.getId());
                questionVo.setQuestion(myQuestion.getQuestion());
                //封装该题选项集合
                QueryWrapper queryWrapper3 = new QueryWrapper();
                queryWrapper3.eq("pid", myQuestion.getId());
                //根据该题的题号，查询该题下所有选项
                List<MyOption> optionlist = myOptionMapper.selectList(queryWrapper3);
                ArrayList<OptionVo> options = new ArrayList<>();
                for (MyOption myOption : optionlist) {
                    OptionVo optionVo = new OptionVo();
                    optionVo.setId(myOption.getId());
                    optionVo.setOption(myOption.getTheoption());
                    options.add(optionVo);
                }
                questionVo.setOptions(options);
                questions.add(questionVo);
            }
            juanziVo.setQuestions(questions);
            result.add(juanziVo);
        }
        return result;
    }

    public ResultVo quertResultReport(Long _id) {
        ArrayList<ResultVo> resultVos = new ArrayList<>();
        ArrayList<Dimension> dimensions = new ArrayList<>();
        Dimension dimension1 = new Dimension();
        dimension1.setKey("外向");
        dimension1.setValue("80%");
        Dimension dimension2 = new Dimension();
        dimension2.setKey("判断");
        dimension2.setValue("70%");
        Dimension dimension3 = new Dimension();
        dimension3.setKey("抽象");
        dimension3.setValue("90%");
        Dimension dimension4 = new Dimension();
        dimension4.setKey("理性");
        dimension4.setValue("60%");
        dimensions.add(dimension1);
        dimensions.add(dimension2);
        dimensions.add(dimension3);
        dimensions.add(dimension4);
        ArrayList<SimilarYou> similarYous = new ArrayList<>();
        SimilarYou similarYou1 = new SimilarYou(1,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_1.png");
        SimilarYou similarYou2 = new SimilarYou(2,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_2.png");
        SimilarYou similarYou3 = new SimilarYou(3,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_3.png");
        SimilarYou similarYou4 = new SimilarYou(4,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_4.png");
        SimilarYou similarYou5 = new SimilarYou(5,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_5.png");
        SimilarYou similarYou6 = new SimilarYou(6,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_6.png");
        SimilarYou similarYou7 = new SimilarYou(7,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_7.png");
        SimilarYou similarYou8 = new SimilarYou(8,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_8.png");
        SimilarYou similarYou9 = new SimilarYou(9,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_9.png");
        SimilarYou similarYou10 = new SimilarYou(10,"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_10.png");
        similarYous.add(similarYou1);
        similarYous.add(similarYou2);
        similarYous.add(similarYou3);
        similarYous.add(similarYou4);
        similarYous.add(similarYou5);
        similarYous.add(similarYou6);
        similarYous.add(similarYou7);
        similarYous.add(similarYou8);
        similarYous.add(similarYou9);
        similarYous.add(similarYou10);
        ResultVo resultVo1 = new ResultVo();

        resultVo1.setConclusion("猫头鹰：他们的共同特质为重计划、条理、细节精准。在行为上，表现出喜欢理性思考与分析、较重视制度、结构、规范。他们注重执行游戏规则、循规蹈矩、巨细靡遗、重视品质、敬业负责。");
        resultVo1.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/owl.png");

        resultVo1.setDimensions(dimensions);
        resultVo1.setSimilarYous(similarYous);
        resultVos.add(resultVo1);

        ResultVo resultVo2 = new ResultVo();
        resultVo2.setConclusion("白兔型：平易近人、敦厚可靠、避免冲突与不具批判性。在行为上，表现出不慌不忙、冷静自持的态度。他们注重稳定与中长程规划，现实生活中，常会反思自省并以和谐为中心，即使面对困境，亦能泰然自若，从容应付。");
        resultVo2.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/rabbit.png");

        resultVo2.setDimensions(dimensions);
        resultVo2.setSimilarYous(similarYous);
        resultVos.add(resultVo2);

        ResultVo resultVo3 = new ResultVo();
        resultVo3.setConclusion("狐狸型 ：人际关系能力极强，擅长以口语表达感受而引起共鸣，很会激励并带动气氛。他们喜欢跟别人互动，重视群体的归属感，基本上是比较「人际导向」。由于他们富同理心并乐于分享，具有很好的亲和力，在服务业、销售业、传播业及公共关系等领域中，狐狸型的领导者都有很杰出的表现。");
        resultVo3.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/fox.png");

        resultVo3.setDimensions(dimensions);
        resultVo3.setSimilarYous(similarYous);
        resultVos.add(resultVo3);

        ResultVo resultVo4 = new ResultVo();
        resultVo4.setConclusion("狮子型：性格为充满自信、竞争心强、主动且企图心强烈，是个有决断力的领导者。一般而言，狮子型的人胸怀大志，勇于冒险，看问题能够直指核心，并对目标全力以赴。他们在领导风格及决策上，强调权威与果断，擅长危机处理，此种性格最适合开创性与改革性的工作。");
        resultVo4.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/lion.png");

        resultVo4.setDimensions(dimensions);
        resultVo4.setSimilarYous(similarYous);
        resultVos.add(resultVo4);

        int id = _id.intValue();
        int index;
        if(id>=56) index=3;
        else if(id>=41&&id<=55) index=2;
        else if(id>=21&&id<=40) index=1;
        else index=0;

        return resultVos.get(index);
    }

    public String queryPush(Answer answer) {
//        int[][] score = new int[][]{{2,4,6},
//                {6,4,7,2,1},
//                {4,2,5,7,6},
//                {4,6,2,1},
//                {6,4,3,5},
//                {6,4,2}};
//        Integer i = Integer.valueOf(answer.getQuestionId())-1;
//        String s = answer.getOptionId();
//        Integer j;
//        if(s.equals("A")) j=0;
//        if(s.equals("B")) j=1;
//        if(s.equals("C")) j=2;
//        if(s.equals("D")) j=3;
//        if(s.equals("E")) j=4;
        int score[] = {15,25,45,65};
        Random random = new Random();
        String result = ""+score[random.nextInt(4)];
        return result;
    }
}
