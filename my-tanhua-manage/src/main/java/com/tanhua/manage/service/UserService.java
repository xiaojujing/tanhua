package com.tanhua.manage.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tanhua.dubbo.server.api.UserLikeApi;
import com.tanhua.manage.enums.LogTypeEnum;
import com.tanhua.manage.enums.SexEnum;
import com.tanhua.manage.mapper.LogMapper;
import com.tanhua.manage.mapper.UserInfoMapper;
import com.tanhua.manage.mapper.UserMapper;
import com.tanhua.manage.pojo.Log;
import com.tanhua.manage.pojo.User;
import com.tanhua.manage.pojo.UserFreeze;
import com.tanhua.manage.pojo.UserInfo;
import com.tanhua.manage.vo.Pager;
import com.tanhua.manage.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService extends ServiceImpl<UserMapper, User> {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private LogMapper logMapper;

    @Autowired
    private UserFreezeService userFreezeService;

    @Reference(version = "1.0.0")
    private UserLikeApi userLikeApi;


    public Pager<UserVo> queryByPage(Integer page, Integer pageSize, Long id, String nickname, String city) {
        IPage<UserVo> iPage = this.userMapper.queryByPage(new Page<>(page, pageSize), Convert.toStr(id), nickname, city);

        //对于结果的处理
        iPage.getRecords().forEach(userVo -> {
            //性别的转化
            userVo.setSex(SexEnum.getSexByValue(Integer.valueOf(userVo.getSex())));

            //用户状态
            userVo.setUserStatus(this.userFreezeService.getFreezeStatusByUserId(userVo.getId()) ? "2" : "1");
        });

        return new Pager<>(iPage);
    }

    public UserVo queryUserInfo(Long userId) {

        UserVo userVo = new UserVo();

        //查询用户的基本信息
        UserInfo userInfo = this.userInfoMapper.selectOne(Wrappers.<UserInfo>lambdaQuery().eq(
                UserInfo::getUserId, userId
        ));

        //数据的拷贝
        BeanUtil.copyProperties(userInfo, userVo);
        userVo.setNickname(userInfo.getNickName());
        BeanUtil.copyProperties(this.userMapper.selectById(userId), userVo);

        //用户状态
        userVo.setUserStatus(this.userFreezeService.getFreezeStatusByUserId(userVo.getId()) ? "2" : "1");

        //粉丝数、喜欢数、配对数
        userVo.setCountLiked(this.userLikeApi.queryLikeCount(userId));
        userVo.setCountBeLiked(this.userLikeApi.queryFanCount(userId));
        userVo.setCountMatching(this.userLikeApi.queryEachLikeCount(userId));


        //最近活跃时间
        Log log = this.logMapper.selectOne(Wrappers.<Log>lambdaQuery()
                .eq(Log::getUserId, userId)
                .orderByDesc(Log::getCreated)
                .last("LIMIT 1")
        );

        if(log != null){
            userVo.setLastActiveTime(log.getCreated().getTime());
        }

        //最近的登录地址
        Log log2 = this.logMapper.selectOne(Wrappers.<Log>lambdaQuery()
                .eq(Log::getUserId, userId)
                .eq(Log::getType, LogTypeEnum.LOGIN.getValue())
                .orderByDesc(Log::getCreated)
                .last("LIMIT 1")
        );

        if(log2 != null){
            userVo.setLastLoginLocation(log2.getPlace());
        }

        return userVo;
    }
}
