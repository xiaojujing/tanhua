package com.tanhua.manage.service;


import com.tanhua.manage.mapper.AdviceMapper;
import com.tanhua.manage.pojo.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @auther liguang
 * @date 2020-12-13 下午 4:05
 * @description
 */
@Service
public class AdviceService {

    @Autowired
    private AdviceMapper adviceMapper;

    //查询净激活数
    public List<Advice> queryAdviceNum(String dateStr, String dateEnd, Integer jiou, Integer gender) {
        return this.adviceMapper.queryAdviceNum(dateStr, dateEnd, jiou, gender);
    }


    //查询注册数
    public List<Advice> queryRegisteredNum(String dateStr, String dateEnd, Integer jiou, Integer gender) {
        return this.adviceMapper.queryRegisteredNum(dateStr, dateEnd, jiou, gender);
    }


    //查询登陆次数
    public List<Advice> queryLogNum(String dateStr, String dateEnd, Integer jiou, Integer gender) {
        return this.adviceMapper.queryLogNum(dateStr, dateEnd, jiou, gender);
    }


    //查询活跃人数
    public List<Advice> queryaAtiveNum(String dateStr, String dateEnd, Integer jiou, Integer gender) {
        return this.adviceMapper.queryaAtiveNum(dateStr, dateEnd, jiou, gender);
    }


}
