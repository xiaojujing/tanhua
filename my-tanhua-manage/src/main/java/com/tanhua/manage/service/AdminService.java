package com.tanhua.manage.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.manage.exception.BusinessException;
import com.tanhua.manage.mapper.AdminMapper;
import com.tanhua.manage.pojo.Admin;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class AdminService extends ServiceImpl<AdminMapper, Admin> {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private static final String CACHE_CODE_PREFIX = "MANAGE_CAP_";

    private static final String CACHE_KEY_TOKEN_PREFIX = "MANAGE_TOKEN_";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Value("${jwt.secret}")
    private String secret;

    public void saveVerificationCode(String uuid, String code) {

        String key = CACHE_CODE_PREFIX + uuid;
        //将验证码保存到redis中，有效时间为5分钟
        this.redisTemplate.opsForValue().set(key, code, Duration.ofMinutes(5));
    }

    public String login(Admin admin, String uuid, String verificationCode) {

        if (StrUtil.isEmpty(admin.getUsername())) {
            throw new BusinessException("用户名不能为空！");
        }

        if (StrUtil.isEmpty(admin.getPassword())) {
            throw new BusinessException("密码不能为空！");
        }

        //校验验证码
        String key = CACHE_CODE_PREFIX + uuid;
        String code = this.redisTemplate.opsForValue().get(key);
        if (!StrUtil.equals(code, verificationCode)) {
            throw new BusinessException("验证码错误！");
        }

        //把原有的验证码删除掉
        this.redisTemplate.delete(key);

        //根据用户名查询admin对象
        Admin oneAdmin = super.lambdaQuery().eq(Admin::getUsername, admin.getUsername()).one();
        //对比密码是否正确
        if (!StrUtil.equals(SecureUtil.md5(admin.getPassword()), oneAdmin.getPassword())) {
            throw new BusinessException("用户名或密码错误！");
        }

        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put("username", oneAdmin.getUsername());
        claims.put("id", oneAdmin.getId());

        // 生成token
        String token = Jwts.builder()
                .setClaims(claims) //设置响应数据体
                .signWith(SignatureAlgorithm.HS256, secret) //设置加密方法和加密盐
                .compact();

        try {
            // 将token存储到redis中
            String redisTokenKey = CACHE_KEY_TOKEN_PREFIX + token;

            //将密码设置为null，不参与序列化
            oneAdmin.setPassword(null);

            String redisTokenValue = MAPPER.writeValueAsString(oneAdmin);
            this.redisTemplate.opsForValue().set(redisTokenKey, redisTokenValue, Duration.ofDays(7));
            return token;
        } catch (Exception e) {
            log.error("存储token出错", e);
            return null;
        }

    }

    public Admin queryUserByToken(String token) {
        String redisTokenKey = CACHE_KEY_TOKEN_PREFIX + token;
        String cacheData = this.redisTemplate.opsForValue().get(redisTokenKey);
        if (StrUtil.isEmpty(cacheData)) {
            return null;
        }

        //刷新时间
        this.redisTemplate.expire(redisTokenKey, 7, TimeUnit.DAYS);

        try {
            return MAPPER.readValue(cacheData, Admin.class);
        } catch (IOException e) {
            log.error("token反序列化出错！" + token);
        }

        return null;
    }

    /**
     * 从redis中删除token缓存
     */
    public void removeToken(String token){
        String redisTokenKey = CACHE_KEY_TOKEN_PREFIX + token;
        this.redisTemplate.delete(redisTokenKey);
    }
}
