package com.tanhua.manage.service;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tanhua.manage.mapper.LogMapper;
import com.tanhua.manage.mapper.RemainingUsersMapper;
import com.tanhua.manage.mapper.UserMapper;
import com.tanhua.manage.pojo.Log;
import com.tanhua.manage.pojo.RemainingUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class RemainingUsersService extends ServiceImpl<RemainingUsersMapper,RemainingUsers> {

    @Autowired
    private RemainingUsersMapper remainingUsersMapper;


    /**
     * 查询当日注册用户数
     * @param dateTime
     * @param offset
     * @return
     */
    public Long queryRegisterUserCount(DateTime dateTime, int offset) {
      return   this.remainingUsersMapper.fingRegisters(dateTime);
    }

    /**
     *
     * 后面皆是查询留存数
     * @param dateTime
     * @param offset
     * @return
     */
    public Long queryOneDayUserCount(DateTime dateTime, int offset) {
        long users = remainingUsersMapper.findUsers(dateTime, DateUtil.offsetDay(dateTime, offset));
        return users;

    }
//    private long queryUserCount (DateTime dateTime ,int offset,String column){
//        RemainingUsers remainingUsers =  super.getOne(Wrappers.<RemainingUsers>query()
//               .select("SUM" +column)  // le 小于等于
//               .le("create", DateUtil.offsetDay(dateTime,offset+1).toDateStr())
//               .ge("create",DateUtil.offsetDay(dateTime,offset).toDateStr()));
//
//        return remainingUsers.getAllUsers();
//    }



    public Long queryTowDayUserCount(DateTime dateTime, int offset) {
        return remainingUsersMapper.findUsers(dateTime,DateUtil.offsetDay(dateTime,offset));
    }

    public Long queryThreeDayUserCount(DateTime dateTime, int offset) {
        return remainingUsersMapper.findUsers(dateTime,DateUtil.offsetDay(dateTime,offset));
    }

    public Long queryFourDayUserCount(DateTime dateTime, int offset) {
        return remainingUsersMapper.findUsers(dateTime,DateUtil.offsetDay(dateTime,offset));
    }

    public Long queryFiveDayUserCount(DateTime dateTime, int offset) {
        return remainingUsersMapper.findUsers(dateTime,DateUtil.offsetDay(dateTime,offset));
    }

    public Long querySixDayUserCount(DateTime dateTime, int offset) {
        return remainingUsersMapper.findUsers(dateTime,DateUtil.offsetDay(dateTime,offset));
    }

    public Long querySevenDayUserCount(DateTime dateTime, int offset) {
        return remainingUsersMapper.findUsers(dateTime,DateUtil.offsetDay(dateTime,offset));
    }

    public Long queryOneMonthUserCount(DateTime dateTime, int offset) {
        return remainingUsersMapper.findUsers(dateTime,DateUtil.offsetDay(dateTime,offset));
    }


    public RemainingUsers queryManyForUsers(DateTime dateTime,  Integer gender) {
        //  获得指定日期是这个日期所在月份的第几天<br>

       return getRemainingUsers(dateTime, gender);
    }

    private RemainingUsers getRemainingUsers(DateTime dateTime,  Integer gender) {
        RemainingUsers remainingUsers = new RemainingUsers();
        //  要查询的日期  即用户注册日期
        remainingUsers.setDate(dateTime.toSqlDate());
        //  当日注册的人数
        remainingUsers.setAllUsers(this.remainingUsersMapper.fingRegisters(dateTime));
        //  后面是1 - 7 天 及30后的留存人数

        remainingUsers.setOneDay(this.remainingUsersMapper.queryManyForUsers(dateTime,DateUtil.offsetDay(dateTime,1),DateUtil.offsetDay(dateTime,2),gender));
        remainingUsers.setTowDay(this.remainingUsersMapper.queryManyForUsers(dateTime,DateUtil.offsetDay(dateTime,2),DateUtil.offsetDay(dateTime,3),gender));
        remainingUsers.setThreeDay(this.remainingUsersMapper.queryManyForUsers(dateTime,DateUtil.offsetDay(dateTime,3),DateUtil.offsetDay(dateTime,4),gender));
        remainingUsers.setFourDay(this.remainingUsersMapper.queryManyForUsers(dateTime,DateUtil.offsetDay(dateTime,4),DateUtil.offsetDay(dateTime,5),gender));
        remainingUsers.setFiveDay(this.remainingUsersMapper.queryManyForUsers(dateTime,DateUtil.offsetDay(dateTime,5),DateUtil.offsetDay(dateTime,6),gender));
        remainingUsers.setSixDay(this.remainingUsersMapper.queryManyForUsers(dateTime,DateUtil.offsetDay(dateTime,6),DateUtil.offsetDay(dateTime,7),gender));
        remainingUsers.setSevenDay(this.remainingUsersMapper.queryManyForUsers(dateTime,DateUtil.offsetDay(dateTime,7),DateUtil.offsetDay(dateTime,7),gender));
        remainingUsers.setOneMonth(this.remainingUsersMapper.queryManyForUsers(dateTime,DateUtil.offsetDay(dateTime,30),DateUtil.offsetDay(dateTime,31),gender));
        return remainingUsers;
    }
}
