package com.tanhua.manage.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.tanhua.manage.pojo.RemainingUsers;
import com.tanhua.manage.service.RemainingUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/system/remaining")
public class RemainingUsersController {

    @Autowired
    private RemainingUsersService remainingUsersService;

    @GetMapping
    // public ResponseEntity<RemainingUsers>  remainingUsers(){
    public List<RemainingUsers> remainingUsers(@RequestParam(name = "startDay", required = false) String startDay,
                                               @RequestParam(name = "endDay", required = false) String endDay
                                              ) {


        DateTime dateTime = null;
        DateTime dateTimeend = null;
        // 创建一个留存用户的集合
        List<RemainingUsers> datas = new ArrayList<>();
        if (startDay == null && endDay == null ) {
            //  正式上线时 使用
            //     DateTime dateTime = DateUtil.date();
            //  由于处于测试阶段   先使用测试数据
            dateTime = DateUtil.parseDate("2020-09-08");
            dateTimeend = DateUtil.parseDate("2020-09-10");
            RemainingUsers remainingUsers = new RemainingUsers();
            RemainingUsers allData = getAllData(dateTime, remainingUsers);
            datas.add(allData);
            while (dateTime.before(dateTimeend)){
                dateTime = DateUtil.offsetDay(dateTime,1);
                RemainingUsers remainingUsersAfter = new RemainingUsers();
                RemainingUsers allDataAfter = getAllData(dateTime, remainingUsersAfter);
                datas.add(allDataAfter);
            }
        }

        if (startDay == null && endDay != null){
            dateTime = DateUtil.parse(endDay, "yyyy-MM-dd");
            RemainingUsers remainingUsers = new RemainingUsers();
            RemainingUsers allData = getAllData(dateTime, remainingUsers);
            datas.add(allData);
        }

        //  这用于判断  查询的开始日期 和 结束日期
        if (startDay != null ) {
            DateTime sDay = DateUtil.parse(startDay, "yyyy-MM-dd");
            RemainingUsers remainingUsers1 = new RemainingUsers();
            RemainingUsers allData1 = getAllData(sDay, remainingUsers1);
            datas.add(allData1);
            // 如果startDay不为空  判断 endDay是否为空  开始日期是否在结束日期之前
            if (endDay != null){
                DateTime eDay = DateUtil.parse(endDay, "yyyy-MM-dd");
                while (sDay.before(eDay)) {
                    // 条件成立 就能期间每一个日期
                    sDay = DateUtil.offsetDay(sDay, 1);
                    RemainingUsers remainingUsers = new RemainingUsers();
                    RemainingUsers allData = getAllData(sDay, remainingUsers);
                    datas.add(allData);
                }
            }




        }


        return datas;
    }

    //  获取单日的数据
    private RemainingUsers getAllData(DateTime dateTime, RemainingUsers remainingUsers) {
        // 获取日期

        remainingUsers.setDate(dateTime.toSqlDate());
        // 获取当日注册的所有用户数
        remainingUsers.setAllUsers(this.remainingUsersService.queryRegisterUserCount(dateTime, 0));
        // 1天后注册留存数
        remainingUsers.setOneDay(this.remainingUsersService.queryOneDayUserCount(dateTime, +1));
        // 2天后注册留存数
        remainingUsers.setTowDay(this.remainingUsersService.queryTowDayUserCount(dateTime, +2));
        // 3天后注册留存数
        remainingUsers.setThreeDay(this.remainingUsersService.queryThreeDayUserCount(dateTime, +3));
        // 4天后注册留存数
        remainingUsers.setFourDay(this.remainingUsersService.queryFourDayUserCount(dateTime, +4));
        // 5天后注册留存数
        remainingUsers.setFiveDay(this.remainingUsersService.queryFiveDayUserCount(dateTime, +5));
        // 6天后注册留存数
        remainingUsers.setSixDay(this.remainingUsersService.querySixDayUserCount(dateTime, +6));
        // 7天后注册留存数
        remainingUsers.setSevenDay(this.remainingUsersService.querySevenDayUserCount(dateTime, +7));
        // 30天后注册留存数
        remainingUsers.setOneMonth(this.remainingUsersService.queryOneMonthUserCount(dateTime, +30));

        return remainingUsers;
    }

    @GetMapping("QueryMany")
    public List<RemainingUsers> QueryDate(@RequestParam(name = "startDay" , required = false ) String startDay,
                          @RequestParam(name = "endDay" , required = false) String endDay,
                          @RequestParam(name = "jiOu" , required = false) Integer jiou,
                          @RequestParam(name = "gender" , required = false) Integer gender){
        // 创建一个 日期集合  用于获取需要的日期
        List<DateTime> DTlist = new ArrayList<>();
        // 创建一个留存用户的集合
        List<RemainingUsers> datas = new ArrayList<>();
        DateTime eDay = null;
        DateTime sDay = null;
        if (startDay == null && endDay == null){
             eDay = DateUtil.date();
             sDay = DateUtil.offsetDay(eDay, -5);
            DTlist.add(sDay);
            while (sDay.before(eDay)){
                sDay = DateUtil.offsetDay(sDay,1);
                DTlist.add(sDay);
            }
        }

        if(startDay != null) {
            sDay = DateUtil.parse(startDay, "yyyy-MM-dd");

            DTlist.add(sDay);
            if (endDay != null) {
                eDay = DateUtil.parse(endDay, "yyyy-MM-dd");
                while (sDay.before(eDay)) {
                    sDay = DateUtil.offsetDay(sDay, 1);
                    DTlist.add(sDay);
                }
            }
        }
        List<DateTime> ouDate = new ArrayList<>();
        List<DateTime> jiDate = new ArrayList<>();
        for (DateTime dateTime : DTlist) {
            if (jiou != null && jiou == 1 && DateUtil.dayOfMonth(dateTime) % 2 != 0 ){
                RemainingUsers remainingUsers = this.remainingUsersService.queryManyForUsers(dateTime,gender);
                datas.add(remainingUsers);
            }
            if (jiou != null && jiou == 2 && DateUtil.dayOfMonth(dateTime) % 2 == 0 ){
                RemainingUsers remainingUsers = this.remainingUsersService.queryManyForUsers(dateTime,gender);
                datas.add(remainingUsers);
            }
            if (jiou == null){
                RemainingUsers remainingUsers = this.remainingUsersService.queryManyForUsers(dateTime,gender);
                datas.add(remainingUsers);
            }

        }
        return datas;
    }

}
