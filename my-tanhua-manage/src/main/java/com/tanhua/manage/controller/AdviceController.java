package com.tanhua.manage.controller;

import com.tanhua.manage.pojo.Advice;

import com.tanhua.manage.util.FindEveryDay;
import com.tanhua.manage.service.AdviceService;
import com.tanhua.manage.vo.AdviceVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.List;

/**
 * @auther liguang
 * @date 2020/12/14 09:40
 * @description
 */
@RestController
@RequestMapping("advice")
@Slf4j
public class AdviceController {

    @Autowired
    private AdviceService adviceService;

    /**
     * 根据时间查询/奇偶查询/性别查询  日数据
     * 日期 净激活数  注册人数 登录次数 活跃人数
     *
     * @param dateStr
     * @param dateEnd
     * @param jiou    1代表奇数天数,2代表偶数天数在SQL语句中查询,如果没有选奇偶代表查询所有
     * @param gender  对应tb_user_info表中字段sex(1代表男,2代表女,3代表未知)
     * @return
     */
    @GetMapping
    public List<AdviceVo> getSummary(@RequestParam(name="dateStr",required = false,defaultValue ="2017-12-15") String dateStr,
                                     @RequestParam(name="dateEnd",required = false,defaultValue = "2020-12-15") String dateEnd,
                                     @RequestParam(name="jiou",required = false) Integer jiou,
                                     @RequestParam(name="gender",required = false) Integer gender) {
        List<AdviceVo> adviceSummaryList = new ArrayList<>();
        //查询净激活人数
        List<Advice> pureList = this.adviceService.queryAdviceNum(dateStr, dateEnd, jiou, gender);
        //查询注册人数
        List<Advice> rigistList = this.adviceService.queryRegisteredNum(dateStr, dateEnd, jiou, gender);
        //查询登录人数
        List<Advice> loginList = this.adviceService.queryLogNum(dateStr, dateEnd, jiou, gender);
        //查询活跃人数
        List<Advice> activeList = this.adviceService.queryaAtiveNum(dateStr, dateEnd, jiou, gender);


        List<String> daysStr = FindEveryDay.findEveryDay(dateStr, dateEnd);
        AdviceVo adviceVo = null;
        for (String day : daysStr) {
            adviceVo = new AdviceVo();
            for (Advice advice : pureList) {
                if (day.equals(advice.getDate())) {
                    adviceVo.setDate(day);
                    adviceVo.setAdviceNum(advice.getAmount());
                }
            }

            for (Advice advice : rigistList) {
                if (day.equals(advice.getDate())) {
                    adviceVo.setRegisteredNum(advice.getAmount());
                }
            }

            for (Advice advice : loginList) {
                if (day.equals(advice.getDate())) {
                    adviceVo.setLogNum(advice.getAmount());
                }
            }

            for (Advice advice : activeList) {
                if (day.equals(advice.getDate())) {
                    adviceVo.setActiveNum(advice.getAmount());
                }
            }
            if (adviceVo.getDate() != null) {
                adviceSummaryList.add(adviceVo);
            }


        }

        return adviceSummaryList;
    }
}
