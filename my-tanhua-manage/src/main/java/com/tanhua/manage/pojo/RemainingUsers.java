package com.tanhua.manage.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor  // 无需联系库表
public class RemainingUsers  {
    private Date date;
    private Long allUsers;
    private Long oneDay;
    private Long towDay;
    private Long threeDay;
    private Long fourDay;
    private Long fiveDay;
    private Long sixDay;
    private Long sevenDay;
    private Long oneMonth;
}
