package com.tanhua.manage.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author lg
 * @date 2020-12-13 下午 9:14
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Advice {

    /**
     * 查询日期
     */
    private String date;
    /**
     * 数量
     */
    private Long amount;
}
