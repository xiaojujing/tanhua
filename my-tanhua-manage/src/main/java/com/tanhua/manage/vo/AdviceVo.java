package com.tanhua.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author lg
 * @date 2020-12-13 下午 7:49
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdviceVo {

    private String date;      //日期
    private Long adviceNum;      //净激活数
    private Long registeredNum;      //注册人数
    private Long logNum;         //登录次数
    private Long activeNum;      //活跃人数

}
