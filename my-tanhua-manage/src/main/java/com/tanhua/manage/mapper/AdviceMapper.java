package com.tanhua.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.manage.pojo.Advice;
import com.tanhua.manage.vo.AdviceVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @auther liguang
 * @date 2020-12-13 下午 4:10
 * @description
 */
@Mapper
public interface AdviceMapper extends BaseMapper<AdviceVo> {

    //查询净激活数
    List<Advice> queryAdviceNum(@Param("start") String dateStr, @Param("end")  String dateEnd, @Param("parity")  Integer jiou, @Param("sex")  Integer gender);

    //查询注册数
    List<Advice> queryRegisteredNum(@Param("start") String dateStr, @Param("end")  String dateEnd, @Param("parity")  Integer jiou, @Param("sex")  Integer gender);

    //查询登陆次数
    List<Advice> queryLogNum(@Param("start") String dateStr, @Param("end")  String dateEnd, @Param("parity")  Integer jiou, @Param("sex")  Integer gender);

    //查询活跃人数
    List<Advice> queryaAtiveNum(@Param("start") String dateStr, @Param("end")  String dateEnd, @Param("parity")  Integer jiou, @Param("sex")  Integer gender);

}
