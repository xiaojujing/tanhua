package com.tanhua.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.manage.pojo.UserFreeze;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserFreezeMapper extends BaseMapper<UserFreeze>{
}