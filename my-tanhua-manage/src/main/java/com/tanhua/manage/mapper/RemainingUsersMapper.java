package com.tanhua.manage.mapper;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.manage.pojo.RemainingUsers;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

@Mapper
public interface RemainingUsersMapper extends BaseMapper<RemainingUsers> {

    // 查询当日注册数
    @Select("SELECT COUNT(id) FROM tb_user WHERE DATE(created) = #{create};")
    long fingRegisters(@Param("create")DateTime create);

     //  查询后续留存数
   // @Select("SELECT COUNT( DISTINCT tl.user_id) FROM tb_log tl LEFT JOIN tb_user tu ON tu.id = tl.user_id  WHERE  DATE(tu.created) = #{create} AND DATE(tl.log_time) = #{logTime} ;")
    long findUsers(@Param("create")DateTime create,@Param("logTime") DateTime logTime);

   // @Select("SELECT COUNT(DISTINCT tl.user_id) FROM tb_log tl LEFT JOIN (SELECT tu.id ,tui.sex FROMtb_user tu LEFT JOIN tb_user_info tui ON tu.id = tui.user_id WHERE DATE(tu.created) = #{create} ) tt ON tl.user_id = tt.id WHERE tl.log_time = #{logTime} AND tt.sex = #{sex};")
    Long queryManyForUsers(@Param("create")DateTime create,@Param("logTime") DateTime logTime,@Param("logTimeEnd") DateTime logTimeEnd , @Param("sex") Integer sex);
}
