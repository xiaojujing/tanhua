package com.tanhua.manage.util;

import cn.hutool.core.date.DateUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lg
 * @date 2020-12-15 上午 9:54
 */
public class FindEveryDay {

    public static List<String> findEveryDay(String dateStr, String dateEnd) {
        Date sDate = DateUtil.parse(dateStr, "yyyy-MM-dd");

        Date eDate = DateUtil.parse(dateEnd, "yyyy-MM-dd");

        List<String> daysStrList = new ArrayList();

        long startTime = sDate.getTime();
        long endTime = eDate.getTime();
        Long oneDay = 1000 * 60 * 60 * 24L;
        Long time = startTime;
        while (time <= endTime) {
            Date d = new Date(time);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            daysStrList.add(df.format(d));
            time += oneDay;
        }

        return daysStrList;
    }
}
