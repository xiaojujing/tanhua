package com.tanhua.manage.huawei;

import com.tanhua.manage.enums.AutoAuditStateEnum;
import com.tanhua.manage.service.HuaWeiUGCService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestHuaWeiUGCService {

    @Autowired
    private HuaWeiUGCService huaWeiUGCService;

    @Test
    public void testTextContentCheck(){
        AutoAuditStateEnum autoAuditStateEnum = this.huaWeiUGCService.textContentCheck("今天心情很开心");
        System.out.println(autoAuditStateEnum.getValue());

        AutoAuditStateEnum autoAuditStateEnum2 = this.huaWeiUGCService.textContentCheck("dsadsadsadsadsadsadsadsa");
        System.out.println(autoAuditStateEnum2.getValue());
    }

    @Test
    public void testImageContentCheck(){
        String[] urls = new String[]{
                "http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg",
                "https://bkimg.cdn.bcebos.com/pic/267f9e2f07082838ac3a8f1bb899a9014c08f18e"
        };
        AutoAuditStateEnum autoAuditStateEnum = this.huaWeiUGCService.imageContentCheck(urls);
        System.out.println(autoAuditStateEnum.getValue());
    }
}